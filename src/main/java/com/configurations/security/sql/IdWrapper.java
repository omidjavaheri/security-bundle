package com.nsa.mha.team3.configurations.security.sql;

public class IdWrapper{

    private @SQLInjectionSafe
    String id;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
