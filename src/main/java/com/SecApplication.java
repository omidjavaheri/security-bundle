package com;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SecApplication {
    public static void main(String[] args) {
        SpringApplication.run(Seclication.class, args);
    }
}
